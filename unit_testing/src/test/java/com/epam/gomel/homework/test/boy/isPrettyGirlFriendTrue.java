package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class isPrettyGirlFriendTrue {
    @Test(groups = "Boy")
    public void  PrettyGirl(){
        Boy boy = new Boy(Month.JULY,3.4);
        Girl girl = new Girl(true,true,boy);
        boy.setGirlFriend(girl);
        Assert.assertEquals(boy.isPrettyGirlFriend(),
                true,"isRich not as expected");
    }
}

