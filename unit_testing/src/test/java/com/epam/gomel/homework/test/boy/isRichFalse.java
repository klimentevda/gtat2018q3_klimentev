package com.epam.gomel.homework.test.boy;


import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;


public class isRichFalse {
    @Test(groups = "Boy")
    public void isRich(){
        Boy boy = new Boy(Month.JULY,3.4);
        Girl girl = new Girl(true,true,boy);
        boy.setGirlFriend(girl);
        Assert.assertEquals(boy.isRich(),
                false,"isRich not as expected");
    }
}

