package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Mood;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.testng.annotations.Test;

import java.time.Month;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.both;

public class MootBad_hamcrest {

    public static Matcher<Boy> getMood(Mood mood) {
        return new FeatureMatcher<Boy, Mood>(equalTo(mood), "Fruit should be round", "Round -") {
            @Override
            protected Mood featureValueOf(Boy fruit) {
                return fruit.getMood();
            }
        };
    }

    public static Matcher<Boy> Rich() {
        return new FeatureMatcher<Boy, Boolean>(is(true), "Fruit should be sweet", "Sweet -") {
            @Override
            protected Boolean featureValueOf(Boy fruit) {
                return fruit.isRich();
            }
        };
    }

    public static Matcher<Boy> SummerMonth() {
        return new FeatureMatcher<Boy, Boolean>(is(true), "Fruit should be sweet", "Sweet -") {
            @Override
            protected Boolean featureValueOf(Boy fruit) {
                return fruit.isRich();
            }
        };
    }

    @Test(groups = "Boy",description = "Rich should")
    public void isRich(){
        Boy fruit = new Boy(Month.JUNE,5000000);

        assertThat(fruit,  both(getMood(Mood.NEUTRAL))
                .and(Rich())
                .and(SummerMonth()));
    }
}


