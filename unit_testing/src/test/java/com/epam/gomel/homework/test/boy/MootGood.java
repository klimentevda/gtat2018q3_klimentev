package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class MootGood {
    @Test(groups = "Boy")
    public void isRich(){
        Boy boy = new Boy(Month.MAY,5000000);
        Girl girl = new Girl(true,true,boy);
        boy.setGirlFriend(girl);
         Assert.assertEquals(boy.isRich(),
                 true,"isRich not as expected");

        Assert.assertEquals(boy.getMood(),
                Mood.GOOD, "mood is not Excellent");
    }
}

