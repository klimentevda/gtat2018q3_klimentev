package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class GirlisBoyfriendRichTrue {
    @Test(groups = "Girl")
    public void FatDetect(){
        Boy boy = new Boy(Month.JULY,5000000);
        Assert.assertEquals(boy.isRich(),
                true,"isRich not as expected");

        Girl girl = new Girl(true,true,boy);

        boy.setGirlFriend(girl);

        Assert.assertEquals(girl.isBoyfriendRich(),
                true,"isRich not as expected");
 }
}
