package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class GirlisBoyFriendWillBuyNewSchoestFalse_priority {
    @Test(groups = "Girl", priority = 2)
    public void FatDetect() {
        Boy boy = new Boy(Month.MAY, 1.4);
        Assert.assertEquals(boy.isRich(),
                false, "isRich not as expected");

        Girl girl = new Girl(true, true, boy);
        boy.setGirlFriend(girl);

        Assert.assertEquals(girl.isBoyFriendWillBuyNewShoes(),
                false, "isBoyFriendWillBuyNewShoes not as expected");
    }
       @Test(groups = "Girl",priority = 1)
        public void FatDetectRichDetect() {
            Boy boy = new Boy(Month.MAY, 5000000);
            Assert.assertEquals(boy.isRich(),
                    true, "isRich not as expected");
           Girl girl = new Girl(false, true, boy);
            boy.setGirlFriend(girl);

            Assert.assertEquals(girl.isBoyFriendWillBuyNewShoes(),
                    false, "isBoyFriendWillBuyNewShoes not as expected");
        }
 }

