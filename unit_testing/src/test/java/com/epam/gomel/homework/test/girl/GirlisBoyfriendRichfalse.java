package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.test.BassTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class GirlisBoyfriendRichfalse extends BassTest {
    @Test(groups = "Girl")
    public void FatDetect(){
        Boy boy = new Boy(Month.JULY,1.4);
        Assert.assertEquals(boy.isRich(),
                false,"isRich not as expected");
        Girl girl = new Girl(true,true,boy);

        boy.setGirlFriend(girl);
        Assert.assertEquals(girl.isBoyfriendRich(),
                false,"isRich not as expected");

        girl = new Girl(true,true);
        Assert.assertEquals(girl.isBoyfriendRich(),
                false,"isRich not as expected");

 }
}
