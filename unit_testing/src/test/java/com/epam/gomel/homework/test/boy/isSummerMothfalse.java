package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class isSummerMothfalse {

    @Test(groups = "Boy")
    public void isSummer(){
        Boy boy = new Boy(Month.MAY);
        Assert.assertEquals(boy.isSummerMonth(),
                false,"isSummerMonth not as expected");
    }
}

