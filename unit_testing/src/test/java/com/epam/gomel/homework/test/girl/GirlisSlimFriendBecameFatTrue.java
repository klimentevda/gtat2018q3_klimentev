package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GirlisSlimFriendBecameFatTrue {
    @Test(groups = "Girl")
    public void FatDetect(){
        Boy boy = new Boy();
        Girl girl = new Girl(false,true,null);
        boy.setGirlFriend(girl);

        Assert.assertEquals(girl.isSlimFriendBecameFat(),
                true,"Fat  not as expected");
 }
}

