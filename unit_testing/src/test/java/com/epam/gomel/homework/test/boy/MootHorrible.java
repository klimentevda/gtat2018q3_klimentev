package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class MootHorrible {
    @Test(groups = "Boy")
    public void isRich(){
        Boy boy = new Boy(Month.MAY,1.2);
        Girl girl = new Girl();
        boy.setGirlFriend(girl);
        Assert.assertEquals(boy.isRich(),
                false,"isRich not as expected");
        Assert.assertEquals(boy.isPrettyGirlFriend(),
                false,"isRich not as expected");
        Assert.assertEquals(boy.getMood(),
                Mood.HORRIBLE, "mood is not Excellent");
    }
}

