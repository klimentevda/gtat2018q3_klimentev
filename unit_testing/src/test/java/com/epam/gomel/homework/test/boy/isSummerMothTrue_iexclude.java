package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class isSummerMothTrue_iexclude {
    @Test(groups = "Boy",dependsOnMethods = "isSummerJULY")
    public void isSummerJUNE() {
        Boy boy = new Boy(Month.JUNE);
        Assert.assertEquals(boy.isSummerMonth(),
                true, "isSummerMonth not as expected");
    }

    @Test(groups = "Boy")
    public void isSummerJULY() {
         Boy boy = new Boy(Month.JULY);
         Assert.assertEquals(boy.isSummerMonth(),
            true, "isSummerMonth not as expected");
    }

    @Test(groups = "Boy")
    public void isSummerAUGUST() {
    Boy boy = new Boy(Month.AUGUST);
    Assert.assertEquals(boy.isSummerMonth(),
        true, "isSummerMonth not as expected");
    }
}


