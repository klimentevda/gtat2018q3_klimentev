package com.epam.gomel.homework;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class MootBad {
    @Test(groups = "Boy")
    public void isRich(){
        Boy boy = new Boy(Month.JUNE,5000000);

        Assert.assertEquals(boy.isSummerMonth(),
                true,"isSummerMonth not as expected");
        Assert.assertEquals(boy.isRich(),
                true,"isRich not as expected");
        Assert.assertEquals(boy.getMood(),
                Mood.NEUTRAL, "mood is not Excellent");
    }
}

