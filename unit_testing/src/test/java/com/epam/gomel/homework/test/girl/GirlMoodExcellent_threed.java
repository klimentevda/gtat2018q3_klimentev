package com.epam.gomel.homework.test.girl;


import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GirlMoodExcellent_threed {
    @Test(threadPoolSize = 10,invocationCount = 9, invocationTimeOut = 11 ,groups = "Girl")
    public void FatDetect(){
        Girl girl = new Girl(false,true);
        Assert.assertEquals(girl.getMood(),
                Mood.NEUTRAL,"mood not as expected");
 }
}

