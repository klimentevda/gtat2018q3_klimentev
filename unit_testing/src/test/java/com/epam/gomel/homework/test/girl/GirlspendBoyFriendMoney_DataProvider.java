package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.test.BassTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class GirlspendBoyFriendMoney_DataProvider extends BassTest {

    @Test(dataProvider = "smCost", groups = "Girl")

    public void spendMoney(double a,double b,double expected){
        Boy boy = new Boy(Month.JULY,a);
        Assert.assertEquals(boy.isRich(),
                true,"Money not as expected");
        Girl girl = new Girl(false,false,boy);
        boy.setGirlFriend(girl);

        girl.spendBoyFriendMoney(b);
        Assert.assertEquals(boy.getWealth(),
                expected,"Money not as expected");

    }
}

