package com.epam.gomel.homework;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GirlisSlimFriendBecameFatFalse {
    @Test(groups = "Girl" )
    public void FatDetect(){
        Boy boy = new Boy();
        Girl girl = new Girl(false,false,boy);
        boy.setGirlFriend(girl);

        Assert.assertEquals(girl.isSlimFriendBecameFat(),
                false,"fat not as expected");


        girl = new Girl(true,true,boy);
        Assert.assertEquals(girl.isSlimFriendBecameFat(),
                false,"Fat not as expected");

 }
}
