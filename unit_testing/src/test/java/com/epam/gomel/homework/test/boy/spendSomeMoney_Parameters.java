package com.epam.gomel.homework.test.boy;


import com.epam.gomel.homework.Boy;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.Month;

public class spendSomeMoney_Parameters {
    @Test(groups = "Boy")
    @Parameters({"Spend","Some"})
    public void spend(Double Spend, Double Some){
        Boy boy = new Boy(Month.JULY,Some);
        boy.spendSomeMoney(Spend);
        Assert.assertEquals(boy.getWealth(),
                300.0,"isRich not as expected");
    }
}

