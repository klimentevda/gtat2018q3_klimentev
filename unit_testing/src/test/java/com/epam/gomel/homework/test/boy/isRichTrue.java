package com.epam.gomel.homework.test.boy;
import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class isRichTrue {
    @Test(groups = "Boy")
    public void isRich(){
        Boy boy = new Boy(Month.JULY,5000000);
        Girl girl = new Girl(true,true);
        boy.setGirlFriend(girl);
        Assert.assertEquals(boy.isRich(),
                true,"isRich not as expected");
    }
}


