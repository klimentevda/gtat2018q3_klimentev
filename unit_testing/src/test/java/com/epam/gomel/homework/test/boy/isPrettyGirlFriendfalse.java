package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class isPrettyGirlFriendfalse {
    @Test(groups = "Boy")
    public void  PrettyGirl(){
        Boy boy = new Boy(Month.JULY,3.4);
        boolean actual = boy.isPrettyGirlFriend();
          Assert.assertEquals(actual,
                  false,"isRich not as expected");

        Girl girl = new Girl(false,true);
        boy = new Boy(Month.JULY,3.4,girl);
        girl.setBoyFriend(boy);
        actual = boy.isPrettyGirlFriend();
        Assert.assertEquals(actual,
                false,"isRich not as expected");

    }
}

