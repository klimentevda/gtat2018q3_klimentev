package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

public class GirlMoodGood {
    @Test(groups = "Girl")
    public void FatDetect(){
        Boy boy = new Boy(Month.JULY,5000000);
        Assert.assertEquals(boy.isRich(),
                true,"mood not as expected");

        Girl girl = new Girl(false,true,boy);
        boy.setGirlFriend(girl);

        Assert.assertEquals(girl.getMood(),
                Mood.GOOD,"mood not as expected");
        //or
        boy = new Boy(Month.JULY,1.4);
        Assert.assertEquals(boy.isRich(),
                false,"mood not as expected");

        girl = new Girl(true,true,boy);
        boy.setGirlFriend(girl);

        Assert.assertEquals(girl.getMood(),
                Mood.GOOD,"mood not as expected");
 }
}

