package com.epam.gomel.homework.test.girl;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GirlisSlimFriendBecameFatFalseMok {
    @Test(groups = "Girl" )
    public void FatDetect(){
        Boy bou = mock(Boy.class);
        when(bou.getWealth()).thenReturn((double) 50000000);

        bou.getWealth();
        Girl girl = new Girl(false,false,bou);

        when(bou.getGirlFriend()).thenReturn(girl);
        bou.setGirlFriend(girl);

        Assert.assertEquals(girl.isSlimFriendBecameFat(),
                false,"fat not as expected");

        girl = new Girl(true,true,bou);
        Assert.assertEquals(girl.isSlimFriendBecameFat(),
                false,"Fat not as expected");

 }
}
