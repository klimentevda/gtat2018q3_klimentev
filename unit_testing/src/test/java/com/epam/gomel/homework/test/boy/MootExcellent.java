package com.epam.gomel.homework.test.boy;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;
import java.util.Iterator;

public class MootExcellent {
    @Test(groups = "Boy")
    public void isRich(){

        Boy boy = new Boy(Month.JULY,5000000);
        Girl girl = new Girl(true);
        boy.setGirlFriend(girl);
        boolean actual = boy.isRich();

        Assert.assertEquals(actual,
                true,"isRich not as expected");
        Assert.assertEquals(boy.getMood(),
                Mood.EXCELLENT, "mood is not Excellent");
    }
}

