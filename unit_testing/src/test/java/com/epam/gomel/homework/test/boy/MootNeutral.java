package com.epam.gomel.homework.test.boy;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.internal.matchers.IsCollectionContaining.hasItem;
import static org.junit.matchers.JUnitMatchers.both;

public class MootNeutral {
    @Test(groups = "Boy")
    public void isRich(){
        Boy boy = new Boy(Month.JUNE,1.4);
        assertThat(boy.isRich() , equalTo(false));


        Assert.assertEquals(boy.isRich(),
                false,"isRich not as expected");

        Assert.assertEquals(boy.getMood(),
                Mood.BAD,"mood is not Excellent");
    }
}

