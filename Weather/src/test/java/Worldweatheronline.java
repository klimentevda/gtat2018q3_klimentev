import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

import static com.jayway.restassured.RestAssured.given;
import static java.lang.System.out;

public class Worldweatheronline extends Date{

    @BeforeTest
    public void buildDom() throws ParserConfigurationException, IOException, SAXException {
        InputStream response = given()./*proxy("http;//127.0.0.1:8888").*/when().get(URL).asInputStream();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.parse(response);
        xpathfactory = XPathFactory.newInstance();
        xPath = xpathfactory.newXPath();
    }

    @Test(priority = 1)
    public void deserializingTest() throws XPathExpressionException, ParseException, IOException {

        NodeList nodesDate = (NodeList)  XmlRepo.parse(data);
        Assert.assertTrue(nodesDate.getLength() > 0);

        NodeList nodesMinC = (NodeList)  XmlRepo.parse(minC);
        Assert.assertTrue(nodesMinC.getLength() > 0);

        NodeList nodesMM = (NodeList)  XmlRepo.parse(MM);
        Assert.assertTrue(nodesMM.getLength() > 0);

        for (i = 0;i < nodesMM.getLength(); i++) {
            if ((filtr.tempMMfilter(i, nodesMM) == true) && (filtr.tempMMfilter(i, nodesMinC) == true)) {
                if (Date.DateMuserF(nodesDate.item(i).getTextContent()).get(Calendar.DAY_OF_WEEK) == 1) {
                    USDate.add(nodesDate.item(i).getTextContent());
                    System.out.print("Date Monday = ");
                    System.out.println(nodesDate.item(i).getTextContent());
                    System.out.println("All days = " + USDate);

                   //PART3
                        for (i = -1; i < 2; i++) {
                           Calendar newDAte =  Date.DateMuserF(nodesDate.item(i).getTextContent());
                            newDAte.add(Calendar.DATE, i);

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                            String formatted = format1.format(newDAte.getTime());
                                    String URL1 = "http://www.nbrb.by/API/ExRates/Rates/145?onDate=" + formatted;
                            Arrays.stream(JSonBul.json(URL1)).map(Example::getCurOfficialRate).forEach(out::println);
                        }
                    }
                }
            }
        }
}

