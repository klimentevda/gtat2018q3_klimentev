

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

    @JsonInclude(JsonInclude.Include.NON_NULL)

    public class SSS implements Serializable {

        @JsonProperty("Cur_ID")
        private long curID;
        @JsonProperty("Cur_ParentID")
        private long curParentID;
        @JsonProperty("Cur_Code")
        private String curCode;
        @JsonProperty("Cur_Abbreviation")
        private String curAbbreviation;
        @JsonProperty("Cur_Name")
        private String curName;
        @JsonProperty("Cur_Name_Bel")
        private String curNameBel;
        @JsonProperty("Cur_Name_Eng")
        private String curNameEng;
        @JsonProperty("Cur_QuotName")
        private String curQuotName;
        @JsonProperty("Cur_QuotName_Bel")
        private String curQuotNameBel;
        @JsonProperty("Cur_QuotName_Eng")
        private String curQuotNameEng;
        @JsonProperty("Cur_NameMulti")
        private String curNameMulti;
        @JsonProperty("Cur_Name_BelMulti")
        private String curNameBelMulti;
        @JsonProperty("Cur_Name_EngMulti")
        private String curNameEngMulti;
        @JsonProperty("Cur_Scale")
        private long curScale;
        @JsonProperty("Cur_Periodicity")
        private long curPeriodicity;
        @JsonProperty("Cur_DateStart")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
        private Date curDateStart;
        @JsonProperty("Cur_DateEnd")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
        private Date curDateEnd;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();


        public long getCurID() {
            return curID;
        }

        public void setCurID(long curID) {
            this.curID = curID;
        }

        public long getCurParentID() {
            return curParentID;
        }

        public void setCurParentID(long curParentID) {
            this.curParentID = curParentID;
        }

        public String getCurCode() {
            return curCode;
        }

        public void setCurCode(String curCode) {
            this.curCode = curCode;
        }

        public String getCurAbbreviation() {
            return curAbbreviation;
        }

        public void setCurAbbreviation(String curAbbreviation) {
            this.curAbbreviation = curAbbreviation;
        }

        public String getCurName() {
            return curName;
        }

        public void setCurName(String curName) {
            this.curName = curName;
        }

        public String getCurNameBel() {
            return curNameBel;
        }

        public void setCurNameBel(String curNameBel) {
            this.curNameBel = curNameBel;
        }

        public String getCurNameEng() {
            return curNameEng;
        }

        public void setCurNameEng(String curNameEng) {
            this.curNameEng = curNameEng;
        }

        public String getCurQuotName() {
            return curQuotName;
        }

        public void setCurQuotName(String curQuotName) {
            this.curQuotName = curQuotName;
        }

        public String getCurQuotNameBel() {
            return curQuotNameBel;
        }

        public void setCurQuotNameBel(String curQuotNameBel) {
            this.curQuotNameBel = curQuotNameBel;
        }

        public String getCurQuotNameEng() {
            return curQuotNameEng;
        }

        public void setCurQuotNameEng(String curQuotNameEng) {
            this.curQuotNameEng = curQuotNameEng;
        }

        public String getCurNameMulti() {
            return curNameMulti;
        }

        public void setCurNameMulti(String curNameMulti) {
            this.curNameMulti = curNameMulti;
        }

        public String getCurNameBelMulti() {
            return curNameBelMulti;
        }

        public void setCurNameBelMulti(String curNameBelMulti) {
            this.curNameBelMulti = curNameBelMulti;
        }

        public String getCurNameEngMulti() {
            return curNameEngMulti;
        }

        public void setCurNameEngMulti(String curNameEngMulti) {
            this.curNameEngMulti = curNameEngMulti;
        }

        public long getCurScale() {
            return curScale;
        }

        public void setCurScale(long curScale) {
            this.curScale = curScale;
        }

        public long getCurPeriodicity() {
            return curPeriodicity;
        }

        public void setCurPeriodicity(long curPeriodicity) {
            this.curPeriodicity = curPeriodicity;
        }

        public Date getCurDateStart() {
            return curDateStart;
        }

        public void setCurDateStart(Date curDateStart) {
            this.curDateStart = curDateStart;
        }

        public Date getCurDateEnd() {
            return curDateEnd;
        }

        public void setCurDateEnd(Date curDateEnd) {
            this.curDateEnd = curDateEnd;
        }

        public Map<String, Object> getAdditionalProperties() {
            return additionalProperties;
        }

        public void setAdditionalProperties(Map<String, Object> additionalProperties) {
            this.additionalProperties = additionalProperties;
        }
    }
