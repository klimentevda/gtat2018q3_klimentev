import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Cur_ID",
        "Date",
        "Cur_Abbreviation",
        "Cur_Scale",
        "Cur_Name",
        "Cur_OfficialRate"
})
public class Example implements Serializable {

    @JsonProperty("Cur_ID")
    private long curID;
    @JsonProperty("Date")
    private String date;
    @JsonProperty("Cur_Abbreviation")
    private String curAbbreviation;
    @JsonProperty("Cur_Scale")
    private long curScale;
    @JsonProperty("Cur_Name")
    private String curName;
    @JsonProperty("Cur_OfficialRate")
    private double curOfficialRate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Cur_ID")
    public long getCurID() {
        return curID;
    }

    @JsonProperty("Cur_ID")
    public void setCurID(long curID) {
        this.curID = curID;
    }

    @JsonProperty("Date")
    public String getDate() {
        return date;
    }

    @JsonProperty("Date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("Cur_Abbreviation")
    public String getCurAbbreviation() {
        return curAbbreviation;
    }

    @JsonProperty("Cur_Abbreviation")
    public void setCurAbbreviation(String curAbbreviation) {
        this.curAbbreviation = curAbbreviation;
    }

    @JsonProperty("Cur_Scale")
    public long getCurScale() {
        return curScale;
    }

    @JsonProperty("Cur_Scale")
    public void setCurScale(long curScale) {
        this.curScale = curScale;
    }

    @JsonProperty("Cur_Name")
    public String getCurName() {
        return curName;
    }

    @JsonProperty("Cur_Name")
    public void setCurName(String curName) {
        this.curName = curName;
    }

    @JsonProperty("Cur_OfficialRate")
    public double getCurOfficialRate() {
        return curOfficialRate;
    }

    @JsonProperty("Cur_OfficialRate")
    public void setCurOfficialRate(double curOfficialRate) {
        this.curOfficialRate = curOfficialRate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("curID", curID).append("date", date).append("curAbbreviation", curAbbreviation).append("curScale", curScale).append("curName", curName).append("curOfficialRate", curOfficialRate).append("additionalProperties", additionalProperties).toString();
    }

}