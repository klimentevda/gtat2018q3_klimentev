import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

public class XmlRepo extends Date{
    public static Object parse(String item) throws XPathExpressionException {
        XPathExpression expr = xPath.compile(Date.XPATH_PATTERN + item);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        return result;
    }
}
