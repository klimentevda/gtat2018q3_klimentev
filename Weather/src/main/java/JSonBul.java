import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.get;

public class JSonBul extends Date {

    public static   Example[]  json(String URL1) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        String aaa = get(URL1).getBody().prettyPrint();
        Example[] allCurrencies = new Example[0];
        allCurrencies = objectMapper.readValue(aaa, Example[].class);
        return allCurrencies;
    }
}
