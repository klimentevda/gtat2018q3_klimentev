Feature: Yandex login
  Check creation of new folder inside

  @all
  Scenario: Successful login with valid credentials
    Given user is on login from
    When user Login in
    Then user is on home page


  Scenario: Successful login with invalid credentials
    Given user is on login from
    When user invalid Login in
    Then user is on home page

  @all
  Scenario: Check creation of new folder inside
    Given user is on login from
    When user Login in
    And user is open disk
    And user create new folder
    And user open new folder
    Then assert for created new folder

  @all
  Scenario: Create New Document Inside That New Folder
    Given user is on login from
    When user Login in
    And user is open disk
    And user create new folder
    And user open new folder
    And user create new document and open
    And user type some word in document
    Then assert for word saved in new document


  @all
  Scenario: Check That You See Document In Trash
    Given user is on login from
    When user Login in
    And user is open disk
    And user create new folder
    And user open new folder
    And user create new document and open
    And user type some word in document
    And user move new document to trash
    Then assert for word in the trash

  @all
  Scenario: Check That Document Removed Completely
    Given user is on login from
    When user Login in
    And user is open disk
    And user create new folder
    And user open new folder
    And user create new document and open
    And user type some word in document
    And user move new document to trash
    And user clean trash
    Then assert trash is clean

  @all
  Scenario: Check works menu items
    Given user is on login from
    When user Login in
    And go to the menu disk
    Then assert that menu disk isDisplayed

  Scenario: Check works menu items
    Given user is on login from
    When user Login in
    And go to the menu Journal
    Then assert that menu Journal isDisplayed

  Scenario: Check works menu items
    Given user is on login from
    When user Login in
    And go to the menu Journal
    Then assert that menu Journal isDisplayed

  Scenario: Check works menu items
    Given user is on login from
    When user Login in
    And go to the menu Photo
    Then assert that menu Photo isDisplayed

  Scenario: Check works menu items
    Given user is on login from
    When user Login in
    And go to the menu Recent
    Then assert that menu Recent isDisplayed

  Scenario: Check works menu items
    Given user is on login from
    When user Login in
    And go to the menu Trash
    Then assert that menu Trash isDisplayed

