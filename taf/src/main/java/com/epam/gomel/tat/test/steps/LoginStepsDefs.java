package com.epam.gomel.tat.test.steps;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.AccountFectory;

import com.epam.gomel.tat.framework.util.WindowsWork;
import com.epam.gomel.tat.yandex.product.logic.disk.CreateNewFileOperation;
import com.epam.gomel.tat.yandex.product.logic.disk.DiskOperation;
import com.epam.gomel.tat.yandex.product.logic.disk.FileTrashOperation;
import com.epam.gomel.tat.yandex.product.pages.disk.WordPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.JournalPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.PhotoPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.RecentPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.TrashPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create.NewFolderPage;
import com.epam.gomel.tat.yandex.product.servise.AccountServise;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.testng.Assert;

public class LoginStepsDefs extends BaseTest {

    @Given("^user is on login from$")
    public void UserIsOnLoginFrom() {
        openBrowser();
    }

    @When("^user Login in$")
    public void UserEntersLogin() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
    }

    @When("^user invalid Login in$")
    public void UserEntersInvalidLogin() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
    }


    @Then("^user is on home page$")
    public void UserIsOnHomePage() {

        String actualTitle = driver.getTitle();
        Assert.assertEquals(
                actualTitle,
                "Яндекс.Паспорт",
                "Page Title not as expected");
    }

    @After
    public void tearDown() {
        downDriver();
    }

    @And("^user is open disk$")
    public void userisOpenDisk() {

        CreateNewFileOperation.openDisk();
    }


    @And("^user create new folder$")
    public void userCreateNewFolder() {
        CreateNewFileOperation.newFolderFile();
    }

    @And("^user open new folder$")
    public void userOpennewFolder() {
        CreateNewFileOperation.openNewFolder();
    }

    @And("^user create new document and open$")
    public void UserCreateNewDocument() {
        CreateNewFileOperation.createWordFile();
    }

    @And("^user type some word in document$")
    public void UserTypeSomeWordInDocument() {
        CreateNewFileOperation.newInputWordinWordFile("Hello world!");
    }

    @Then("^assert for word saved in new document$")
    public void AssertForWordSavedInWewDocument() {
        WindowsWork.qthePage();
        CreateNewFileOperation.openLastWordFile();
        Assert.assertEquals(
                WordPage.paragrafGet().getText(),
                "Hello world! ",
                "New word file have invalid value");
    }

    @And("^user move new document to trash$")
    public void moveElementToTrash() {
        FileTrashOperation.moveElementtoTrash();
    }


    @Then("^assert for word in the trash$")
    public void AssertForWordInTheTrash() {
        Assert.assertEquals(
                TrashPage.checkItemsFile().getText(),
                "",
                "New word file don`t be in trash");

        DiskOperation.choiceNavigateItem("trash");
        TrashPage.lastWord().getText();
        Assert.assertEquals(
                TrashPage.lastWord().getText(),
                "Документ.\ndocx",
                "New word file don`t be in trash");
    }

    @And("^user clean trash$")
    public void userCleanTrash() {
        FileTrashOperation.cleanTrash();
    }

    @Then("^assert trash is clean$")
    public void assertTrashInClean() {
        Assert.assertEquals(
                TrashPage.checkItemsFile().getText(),
                "",
                "New word file don`t be in trash");
    }


    @Then("^assert that menu disk isDisplayed$")
    public void assertThatMenuDiskIsDisplayed() {
        Assert.assertEquals(
                RecentPage.titleItemPage().getText(),
                "Файлы",
                "disk page not as expected");
    }

    @And("^go to the menu Journal$")
    public void goToTheMenuJournal() throws Throwable {
        DiskOperation.choiceNavigateItem("journal");
    }


    @And("^go to the menu disk$")
    public void goToTheMenuDisk() {
        DiskOperation.choiceNavigateItem("disk");

    }

    @Then("^assert that menu Journal isDisplayed$")
    public void assertThatMenuJournalIsDisplayed() {

        Assert.assertEquals(
                JournalPage.titleItemPage().getText(),
                "История",
                "disk page not as expected");
    }

    @And("^go to the menu Photo$")
    public void goToTheMenuPhoto() {
        DiskOperation.choiceNavigateItem("photo");
    }

    @Then("^assert that menu Photo isDisplayed$")
    public void assertThatMenuPhotoIsDisplayed() {
        Assert.assertEquals(
                PhotoPage.titleItemPage().getText(),
                "Все фотографии",
                "disk page not as expected");
    }

    @And("^go to the menu Recent$")
    public void goToTheMenuRecent() {
        DiskOperation.choiceNavigateItem("recent");
    }

    @Then("^assert that menu Recent isDisplayed$")
    public void assertThatMenuRecentIsDisplayed() throws Throwable {
        Assert.assertEquals(
                RecentPage.titleItemPage().getText(),
                "Последние файлы",
                "recent page not as expected");
    }

    @And("^go to the menu Trash$")
    public void goToTheMenuTrash() {
        DiskOperation.choiceNavigateItem("trash");
    }

    @Then("^assert that menu Trash isDisplayed$")
    public void assertThatMenuTrashIsDisplayed() {
        Assert.assertEquals(
                TrashPage.titleItemPage().getText(),
                "Корзина",
                "disk page not as expected");
    }

    @Then("^assert for created new folder$")
    public void assertForCreatedNewFolder(){
        Assert.assertEquals(
                CreateNewFileOperation.getNameGen(),
                NewFolderPage.newNameFolder().getText(),
                "New Folder don`t create");
    }
}
