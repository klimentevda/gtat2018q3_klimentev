package com.epam.gomel.tat.yandex.product.pages.disk;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WordPage extends BaseTest {

    private static WebElement element;

    public static WebElement status() {
        element = driver.findElement(By.xpath("//span[@id='BreadcrumbSaveStatus']"));
        return element;
    }

    public static WebElement stringWord() {
        element = driver.findElement(By.xpath("//div[@class = 'OutlineGroup']//span"));
        return element;
    }

    public static WebElement paragrafGet() {
        element = driver.findElement(By.xpath("//p[@class = 'Paragraph']"));
        return element;
    }

    public static WebElement linkEdit() {
        element = driver.findElement(By.xpath("//div[contains(@class, 'hover-tooltip')]//a"));
        return element;
    }
}
