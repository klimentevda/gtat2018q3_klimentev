package com.epam.gomel.tat.yandex.product.pages.disk;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class UserPageYandex extends BaseTest {

    private static WebElement element;

    public static WebElement goToServices() {
        element = driver.findElement(By.xpath("//a[@href = '/profile/services']"));
        return element;
    }
}
