package com.epam.gomel.tat.framework.screen;

import com.epam.gomel.tat.framework.ul.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class BasePage {

    public static  WebDriver driver;

    public static String url = "https://passport.yandex.ru/profile/services";
    public static String userName = "d3nistest";
    public static String userPassword = "serk19485";
    public static String userInvolidPassword = "involid";

    public BasePage() {
        PageFactory.initElements(driver, this);
        driver = setUpDriver(url);
    }

    private static WebDriver setUpDriver(String urlSt) {
        if (driver == null) {
            driver = BrowserFactory.getBrowser();
            driver.get(urlSt);
        }
        return driver;
    }

    public static WebDriver downDriver() {
        //Close browser, destroy selenium session
        if (driver != null) {
            driver.quit();
            driver = null;
        }
        return null;
    }
}
