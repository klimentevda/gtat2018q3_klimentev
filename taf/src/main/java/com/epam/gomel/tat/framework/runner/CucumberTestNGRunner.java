package com.epam.gomel.tat.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;



@CucumberOptions(
        strict = true,
        features = "src/main/resources/features",
        glue = "com.epam.gomel.tat.test.steps",
        tags = "@all",
        plugin = {"json:target/Cucumber.json", "html:target/cucumber-report"

        }
)

public class CucumberTestNGRunner extends AbstractTestNGCucumberTests {
}
