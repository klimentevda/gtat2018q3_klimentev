package com.epam.gomel.tat.framework.util;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.loger.Log;
import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


import java.io.File;
import java.io.IOException;

public class Brouwser extends BaseTest {

    public static void makeScreenShot() {
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + "screenshot.png");
        try {
            File ScreenshotASFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(ScreenshotASFile, screenshotFile);
            Log.info("<a href='" + screenshotFile + "' target='blank'>screenshot.file</a>");
        } catch (IOException e) {
            throw new RuntimeException("Failed screenshot", e);
        }
    }
}
