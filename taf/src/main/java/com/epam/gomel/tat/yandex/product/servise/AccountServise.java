package com.epam.gomel.tat.yandex.product.servise;


import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.Account;
import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.util.Wait;
import com.epam.gomel.tat.yandex.product.pages.LoginPageYandex;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountServise extends BaseTest {

    public static WebElement dynamicElement;
    String idLevel = LoginPageYandex.idLevel().getAttribute("class");

    public AccountServise sinlIn(Account account) {
        if (idLevel.equals("layout")) {
            Log.info("Login page Have = " + idLevel);
            LoginPageYandex.userName2().sendKeys(account.getLogin());
            LoginPageYandex.userButtonIn2().click();
            dynamicElement = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name= 'passwd']")));
            Wait.waitForJSLoads();
            Log.info("Whait end click" + LoginPageYandex.userPass());
            LoginPageYandex.userPass().click();
            LoginPageYandex.userPass().sendKeys(account.getPassword());
            Log.info("Click" + LoginPageYandex.userButtonIn2());
            LoginPageYandex.userButtonIn2().click();
            Wait.waitForJSLoads();

        } else {
            Log.info("Login page dos`t have = " + idLevel);
            Log.info("Click" + LoginPageYandex.checkbox());
            LoginPageYandex.checkbox().click();
            LoginPageYandex.userName().sendKeys(account.getLogin());
            Wait.waitForJSLoads();
            LoginPageYandex.userPass().sendKeys(account.getPassword());
            Log.info("Click" + LoginPageYandex.userButtonIn());
            LoginPageYandex.userButtonIn().click();
            Wait.waitForJSLoads();
        }
        return null;
    }
}
