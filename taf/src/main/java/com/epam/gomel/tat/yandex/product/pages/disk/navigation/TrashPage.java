package com.epam.gomel.tat.yandex.product.pages.disk.navigation;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TrashPage extends BaseTest {

    private static WebElement element;

    public static WebElement titleItemPage() {
        element = driver.findElement(By.xpath("//div[@class= 'listing-head']//h1"));
        return element;
    }

    public static WebElement lastWord() {
        element = driver.findElement(By.xpath("//span[@class= 'clamped-text']/ancestor::div[3]"));
        return element;
    }


    public static WebElement burronCleanTrash() {
        element = driver.findElement(By.xpath("//button[contains(@class, 'clean-trash-button')]"));
        return element;
    }

    public static WebElement burronCleanTrashTrue() {
        element = driver.findElement(By.xpath("//span[text() = 'Очистить']"));
        return element;
    }

    public static WebElement checkItemsFile() {
        element = driver.findElement(By.xpath("//div[contains(@class, 'listing__items')]"));
        return element;
    }

    public static WebElement trashDetect() {
        element = driver.findElement(By.xpath("//div[contains(@data-key, 'boxProgressbars')]"));
        return element;
    }
}
