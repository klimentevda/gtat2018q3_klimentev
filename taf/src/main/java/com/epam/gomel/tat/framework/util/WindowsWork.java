package com.epam.gomel.tat.framework.util;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.yandex.product.pages.disk.WordPage;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class WindowsWork extends BaseTest {
    public static ArrayList<String> tabsWindows;
    public static WebDriverWait wait = new WebDriverWait(driver, 10);

    public static void choseThePage() {
        tabsWindows = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabsWindows.get(1));
    }

    public static void choseTheframe() {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
        try {
            Log.info("Try to click" + WordPage.stringWord());
            WordPage.stringWord().click();
        } catch (WebDriverException ex) {
            try {
                Log.info("Sleep 3000");
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        wait.until(ExpectedConditions.elementToBeClickable(WordPage.stringWord()));
    }

    public static void qthePage() {
        driver.switchTo().defaultContent();
        driver.close();
        driver.switchTo().window(tabsWindows.get(0));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
}
