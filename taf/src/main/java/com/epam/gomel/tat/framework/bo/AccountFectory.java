package com.epam.gomel.tat.framework.bo;

import com.epam.gomel.tat.core.BaseTest;

public class AccountFectory extends BaseTest {

    public static Account defoultAccount() {
        Account account = new Account();
        account.setLogin(userName);
        account.setPassword(userPassword);
        return account;
    }

    public static Account wrongAccount() {
        Account account = new Account();
        account.setLogin(userName);
        account.setPassword(userInvolidPassword);
        return account;
    }

}
