package com.epam.gomel.tat.framework.bo;

public class AccountBullder {

    private Account account;

    public AccountBullder login(String login) {
        account.setLogin(login);
        return this;
    }

    public AccountBullder password(String password) {
        account.setPassword(password);
        return this;
    }

    public Account build() {
        return account;
    }
}
