package com.epam.gomel.tat.yandex.product.pages.disk.navigation;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SharedPage extends BaseTest {

    private static WebElement element;

    public static WebElement titleItemPage() {
        element = driver.findElement(By.xpath("//div[@class= 'listing-head']//h1"));
        return element;
    }
}
