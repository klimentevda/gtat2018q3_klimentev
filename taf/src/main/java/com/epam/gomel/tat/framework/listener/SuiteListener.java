package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.loger.Log;
import org.testng.ISuite;
import org.testng.ITestNGListener;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class SuiteListener implements ITestNGListener {

    @BeforeSuite
    public void onStart(ISuite iSuite) {
        Log.info("[SUITE STARTED]" + iSuite.getName());
    }

    @AfterSuite
    public void onFinish(ISuite iSuite) {
        Log.info("[SUITE FINISHED]" + iSuite.getName());
    }
}
