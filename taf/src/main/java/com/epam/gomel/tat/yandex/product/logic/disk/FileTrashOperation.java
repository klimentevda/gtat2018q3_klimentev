package com.epam.gomel.tat.yandex.product.logic.disk;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.util.Wait;
import com.epam.gomel.tat.yandex.product.pages.disk.UserDiskPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.TrashPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create.NewFolderPage;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class FileTrashOperation extends BaseTest {

    public static Actions builder = new Actions(driver);
    public static WebDriverWait wait = new WebDriverWait(driver, 10);

    public static void moveElementtoTrash() {
        wait.until(ExpectedConditions.visibilityOfAllElements(NewFolderPage.lastWordMove()));
        wait.until(ExpectedConditions.elementToBeClickable(NewFolderPage.lastWordMove()));
        wait.until(ExpectedConditions.visibilityOfAllElements(UserDiskPage.navigate("trash")));
        wait.until(ExpectedConditions.elementToBeClickable(UserDiskPage.navigate("trash")));
        Wait.waitForJSLoads();
        try {
            Action dragAndDrop = builder
                    .click(NewFolderPage.lastWordMove())
                    .clickAndHold(NewFolderPage.lastWordMove())
                    .moveToElement(UserDiskPage.navigate("trash"))
                    .release(UserDiskPage.navigate("trash"))
                    .build();
            Wait.waitForJSLoads();
            dragAndDrop.perform();
            Wait.waiteString(TrashPage.trashDetect(), "");
            Log.debug("Screen");
        } catch (WebDriverException ex) {
            try {
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Action dragAndDrop = builder
                    .click(NewFolderPage.lastWordMove())
                    .clickAndHold(NewFolderPage.lastWordMove())
                    .moveToElement(UserDiskPage.navigate("trash"))
                    .release(UserDiskPage.navigate("trash"))
                    .build();
            dragAndDrop.perform();
            Wait.waitForJSLoads();
            Wait.waiteString(TrashPage.trashDetect(), "");
            Log.debug("Screen");
        }
    }

    public static void cleanTrash() {
        Wait.waitForJSLoads();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.elementToBeClickable(TrashPage.burronCleanTrash()));
        Wait.waitEndClic(TrashPage.burronCleanTrash());
        Wait.waitEndClic(TrashPage.burronCleanTrashTrue());
        Wait.waiteString(TrashPage.trashDetect(), "");
        Wait.waitForJSLoads();
        Log.debug("Screen");
    }
}
