package com.epam.gomel.tat.framework.loger;


import org.apache.log4j.Logger;
import com.epam.gomel.tat.framework.ul.Brouwser;
public class Log {

    public static Logger logger = Logger.getLogger("MyLoger");

    public static void debug(String message) {
        logger.debug(message);
        Brouwser.makeScreenShot();
    }

    public static void info(String message) {
        logger.info(message);
    }

    public static void error(String message) {
        logger.error(message);
    }
}
