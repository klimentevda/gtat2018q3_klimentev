package com.epam.gomel.tat.framework.util;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.loger.Log;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

public class Wait extends BaseTest {

    public static JavascriptExecutor executor = (JavascriptExecutor) driver;
    public static WebDriverWait wait = new WebDriverWait(driver, 10);


    public static boolean waitForJSLoads() {
        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    public static void waiteString(WebElement element, String whatAreYouWhontToSee) {
        int time = 8000;
        String index = element.getText();
        boolean indexvalue = index.equals(whatAreYouWhontToSee);
        while (indexvalue == false) {
            Log.info(index + " = false");
            if (time != 0) {
                try {
                    Log.info("Time OuT =" + time);
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                time = time - 1000;
                Log.info("new time out = " + time);
            }

            index = element.getText();
            indexvalue = index.equals(whatAreYouWhontToSee);
            Log.info("New index = " + index);
        }
    }

    public static void waitEndClic(WebElement element) {

        try {
            Log.info("Try to click" + element);
            executor.executeScript("arguments[0].click();", element);
        } catch (WebDriverException ex) {
            Log.info("wait and click" + element);
            executor.executeScript("if ((window.scrollY!=0) || (window.scrollX!=0)) window.scrollTo(0, 0);");
            wait.until(ExpectedConditions.visibilityOfAllElements(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            executor.executeScript("arguments[0].click();", element);
        }
    }

    public static void waitEndClear(WebElement element) {
        Log.info("wait end clear" + element);
        wait.until(ExpectedConditions.visibilityOfAllElements(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.clear();
    }

    public static void waitEndSendKey(WebElement element, String text) {
        try {
            Log.info("Try tu Send Key with" + element + "to the text:" + text);
            element.sendKeys(text);
        } catch (WebDriverException ex) {
            Log.info("wait end Send Key with" + element + "to the text:" + text);
            wait.until(ExpectedConditions.visibilityOfAllElements(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.sendKeys(text);
        }
    }
}
