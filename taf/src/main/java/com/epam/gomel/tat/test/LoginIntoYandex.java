package com.epam.gomel.tat.test;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.AccountFectory;
import com.epam.gomel.tat.framework.util.Wait;
import com.epam.gomel.tat.yandex.product.pages.LoginPageYandex;
import com.epam.gomel.tat.yandex.product.servise.AccountServise;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginIntoYandex extends BaseTest {
    /**
     * Test login into yandex disk using credentials (positive and negative checks
     * positive
     */
    @Test
    public static void loginIntoYandexDiskPositive() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        String actualTitle = driver.getTitle();
        Assert.assertEquals(
                actualTitle,
                "Яндекс.Паспорт",
                "Page Title not as expected");
    }

    /**
     * Test login into yandex disk using credentials (positive and negative checks)
     * negative
     */
    @Test
    public static void loginIntoYandexDiskNegative() {
        new AccountServise().sinlIn(AccountFectory.wrongAccount());
        Wait.waitForJSLoads();
        //WebDriverWait wait = new WebDriverWait(driver, 10);
        //wait.until(ExpectedConditions.visibilityOfAllElements(LoginPageYandex.errorInputValue()));
        String idLevel = LoginPageYandex.idLevel().getAttribute("class");
        if (idLevel.equals("layout")) {
            Assert.assertEquals(
                    LoginPageYandex.errorInputValue().getText(),
                    "Неверный пароль. Не помню пароль",
                    "Mistake input in passt");
        } else {
            Assert.assertEquals(
                    LoginPageYandex.errorInputValue().getText(),
                    "Неверный пароль",
                    "Mistake input in passt");
        }
    }
}
