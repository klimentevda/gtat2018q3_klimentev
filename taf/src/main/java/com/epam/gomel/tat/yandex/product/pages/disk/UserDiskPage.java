package com.epam.gomel.tat.yandex.product.pages.disk;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class UserDiskPage extends BaseTest {

    private static WebElement element;

    public static WebElement goUserDropDown() {
        element = driver.findElement(By.xpath("//div[contains(@class, 'user2')]"));
        return element;
    }

    public static WebElement goOutButton() {
        element = driver.findElement(By.xpath("//div[contains(@class, 'user2')]//li/a[contains(@href, 'logout')]"));
        return element;
    }

    //navigate
    public static WebElement navigate(String item) {
        element = driver.findElement(By.xpath("//div[contains(@class, 'navigation')]/a[@href='/client/" + item + "']"));
        return element;
    }
}
