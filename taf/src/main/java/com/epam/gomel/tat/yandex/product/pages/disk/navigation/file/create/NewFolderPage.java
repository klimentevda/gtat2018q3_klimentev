package com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewFolderPage extends BaseTest {

    private static WebElement element;

    public static WebElement newNameFolder() {
        element = driver.findElement(By.xpath("//div[contains(@class, 'listing-heading')]/h1"));
        return element;
    }

    public static WebElement lastWord() {
        element = driver.findElement(By.xpath("//span[@class= 'clamped-text']"));
        return element;
    }

    public static WebElement lastWordMove() {
        element = driver.findElement(By.xpath("//span[@class= 'clamped-text']/ancestor::div[3]"));
        return element;
    }

    public static WebElement newButton() {
        element = driver.findElement(By.xpath("//span[contains(@class, 'create')]//button"));
        return element;
    }

    public static WebElement newWordDocument() {
        element = driver.findElement(By.xpath(" //button[contains(@class, 'create-item')][2]"));
        return element;
    }
}
