package com.epam.gomel.tat.test.disk;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.AccountFectory;
import com.epam.gomel.tat.framework.util.WindowsWork;
import com.epam.gomel.tat.yandex.product.logic.disk.CreateNewFileOperation;
import com.epam.gomel.tat.yandex.product.pages.disk.WordPage;
import com.epam.gomel.tat.yandex.product.servise.AccountServise;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateNewDocumentInsideThatNewFolder extends BaseTest {
    /**
     * Create new Word document inside that new folder, give it name, past some text inside document
     * (“Hello world!” for example) and save it
     * Check that you can see document inside appropriate folder
     * and you can reopen it for editing and you see all you information saved correctly
     */
    @Test
    public static void newFile() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        CreateNewFileOperation.newFolderFile();
        CreateNewFileOperation.openNewFolder();
        CreateNewFileOperation.createWordFile();
        CreateNewFileOperation.newInputWordinWordFile("Hello world!");
        WindowsWork.qthePage();
        CreateNewFileOperation.openLastWordFile();
        Assert.assertEquals(
                WordPage.paragrafGet().getText(),
                "Hello world! ",
                "New word file have invalid value");
    }
}
