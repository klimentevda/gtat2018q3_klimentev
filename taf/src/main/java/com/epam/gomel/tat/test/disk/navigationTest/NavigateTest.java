package com.epam.gomel.tat.test.disk.navigationTest;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.AccountFectory;
import com.epam.gomel.tat.yandex.product.logic.disk.CreateNewFileOperation;
import com.epam.gomel.tat.yandex.product.logic.disk.DiskOperation;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.JournalPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.PhotoPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.RecentPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.TrashPage;
import com.epam.gomel.tat.yandex.product.servise.AccountServise;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NavigateTest extends BaseTest {
    /**
     * Check that all main menu items works correctly and lead tocorrect page:
     * Файлы
     */
    @Test(groups = "Navigation")
    public static void navigateMenuDisk() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        DiskOperation.choiceNavigateItem("disk");
        Assert.assertEquals(
                RecentPage.titleItemPage().getText(),
                "Файлы",
                "disk page not as expected");
    }

    /**
     * Check that all main menu items works correctly and lead to correct page:
     * Последние, Файлы, Фото, Общий доступ, История, Архив, Корзина
     */
    @Test(groups = "Navigation")
    public static void navigateMenuJournal() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        DiskOperation.choiceNavigateItem("journal");
        Assert.assertEquals(
                JournalPage.titleItemPage().getText(),
                "История",
                "disk page not as expected");
    }

    /**
     * Check that all main menu items works correctly and lead to correct page:
     * Фото
     */
    @Test(groups = "Navigation")
    public static void navigateMenuPhoto() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        DiskOperation.choiceNavigateItem("photo");
        Assert.assertEquals(
                PhotoPage.titleItemPage().getText(),
                "Все фотографии",
                "disk page not as expected");
    }

    /**
     * Check that all main menu items works correctly and lead to correct page:
     * Последние
     */
    @Test(groups = "Navigation")
    public static void navigateMenuRecent() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        DiskOperation.choiceNavigateItem("recent");
        Assert.assertEquals(
                RecentPage.titleItemPage().getText(),
                "Последние файлы",
                "recent page not as expected");
    }

    /**
     * Check that all main menu items works correctly and lead to correct page:
     * Архив
     */
    @Test(groups = "Navigation")
    public static void navigateMenuTrash() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        DiskOperation.choiceNavigateItem("trash");
        Assert.assertEquals(
                TrashPage.titleItemPage().getText(),
                "Корзина",
                "disk page not as expected");
    }
}
