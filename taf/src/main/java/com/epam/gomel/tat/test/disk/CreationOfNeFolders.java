package com.epam.gomel.tat.test.disk;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.AccountFectory;
import com.epam.gomel.tat.yandex.product.logic.disk.CreateNewFileOperation;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create.NewFolderPage;
import com.epam.gomel.tat.yandex.product.servise.AccountServise;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreationOfNeFolders extends BaseTest {
    /**
     * Check creation of new folder inside Файлы (use unique name for each new folder)
     * check you can visit that folder
     */
    @Test
    public static void newFolder() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        CreateNewFileOperation.newFolderFile();
        CreateNewFileOperation.openNewFolder();
        Assert.assertEquals(
                CreateNewFileOperation.getNameGen(),
                NewFolderPage.newNameFolder().getText(),
                "New Folder don`t create");
    }
}
