package com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateNewFolderPage extends BaseTest {

    private static WebElement element;

    public static WebElement newNameFolder() {
        element = driver.findElement(By.xpath("//div[@class = 'dialog__body']//input"));
        return element;
    }

    public static WebElement saveButtonnewFolder() {
        element = driver.findElement(By.xpath("//div[@class = 'confirmation-dialog__footer']//button[1]"));
        return element;
    }
}
