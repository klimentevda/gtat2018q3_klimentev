package com.epam.gomel.tat.yandex.product.pages;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPageYandex extends BaseTest {

    private static WebElement element;

    public static WebElement userName() {
        element = driver.findElement(By.xpath("//input[@name = 'login']"));
        return element;
    }

    public static WebElement userName2() {
        element = driver.findElement(By.xpath("//input[@name = 'login']"));
        return element;
    }

    public static WebElement userPass() {
        element = driver.findElement(By.xpath("//input[@name= 'passwd']"));
        return element;
    }

    public static WebElement userButtonIn() {
        element = driver.findElement(By.xpath("//button[@class = 'passport-Button']"));
        return element;
    }

    public static WebElement userButtonIn3() {
        element = driver.findElement(By.xpath("//span[text() = 'Войти']"));
        return element;
    }

    public static WebElement userButtonIn2() {
        element = driver.findElement(By.xpath("//button[contains(@class, 'button2')]"));
        return element;
    }

    public static WebElement checkbox() {
        element = driver.findElement(By.xpath("//label[@class='passport-Checkbox']//span[1]"));
        return element;
    }

    public static WebElement errorInputValue() {
        element = driver.findElement(By.xpath("//div[contains(text(), 'Неверный')]"));
        return element;
    }

    public static WebElement continueLog() {
        element = driver.findElement(By.xpath("//div[contains(@class, 'passp')]//h2"));
        return element;
    }

    public static WebElement idLevel() {
        element = driver.findElement(By.xpath("//div[@id='root']"));
        return element;
    }
}
