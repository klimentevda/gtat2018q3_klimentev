package com.epam.gomel.tat.core;

import com.epam.gomel.tat.framework.screen.BasePage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest extends BasePage {

    @BeforeMethod
    public void openBrowser() {
        new BasePage();
    }

    @AfterMethod
    public void closeBrowser() {
        //Close browser, destroy selenium session
        downDriver();
    }
}
