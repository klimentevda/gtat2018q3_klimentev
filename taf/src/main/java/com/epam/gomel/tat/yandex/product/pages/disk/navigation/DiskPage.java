package com.epam.gomel.tat.yandex.product.pages.disk.navigation;

import com.epam.gomel.tat.core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DiskPage extends BaseTest {

    private static WebElement element;

    public static WebElement titleItemPage() {
        element = driver.findElement(By.xpath("//section[contains(@class, 'listing')]//h1"));
        return element;
    }

    public static WebElement newButton() {
        element = driver.findElement(By.xpath("//span[contains(@class, 'create')]//button"));
        return element;
    }

    public static WebElement newFolder() {
        element = driver.findElement(By.xpath(" //button[contains(@class, 'create-item')][1]"));
        return element;
    }

    public static WebElement newWordDocument() {
        element = driver.findElement(By.xpath(" //button[contains(@class, 'create-item')][2]"));
        return element;
    }

    public static WebElement openNewFolder(String folderName) {
        element = driver.findElement(By.xpath("//span[text() = '" + folderName + "']"));
        return element;
    }

    public static WebElement openNewFolderClick(String folderName) {
        element = driver.findElement(By.xpath("//span[text() = '" + folderName + "']/ancestor::div[3]/div"));
        return element;
    }

    public static WebElement sortItem() {
        element = driver.findElement(By.xpath("//span[contains(@class, 'listing-sort')]"));
        return element;
    }

    public static WebElement dateSortItem() {
        element = driver.findElement(By.xpath("//*[text() = 'Дате изменения']"));
        return element;
    }

    public static WebElement sortItemDown() {
        element = driver.findElement(By.xpath("//*[text() = 'Убыванию']"));
        return element;
    }

    public static WebElement detectNewFolder() {
        element = driver.findElement(By.xpath("//div[@data-key = 'view=notifications']"));
        return element;
    }
}
