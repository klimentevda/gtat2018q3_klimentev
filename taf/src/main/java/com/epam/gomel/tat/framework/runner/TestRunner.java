package com.epam.gomel.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.gomel.tat.framework.listener.SuiteListener;
import com.epam.gomel.tat.framework.listener.TestListener;
import com.epam.gomel.tat.framework.loger.Log;
import org.testng.TestNG;

import java.util.Collections;

public class TestRunner {

    private static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new SuiteListener());
        testNG.addListener(new TestListener());
        testNG.setTestSuites(Collections.singletonList("./testNGRunner.xml"));
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse cli");
        parseCLi(args);
        Log.info("Start app");
        createTestNG().run();
        Log.info("Finish app");
    }

    private static void parseCLi(String[] args) {
        Log.info("Parese clis using JCommander");
        JCommander JCommander = new JCommander(Parameters.instance());
        try {
            JCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage());
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            JCommander.usage();
            System.exit(0);
        }
    }
}


