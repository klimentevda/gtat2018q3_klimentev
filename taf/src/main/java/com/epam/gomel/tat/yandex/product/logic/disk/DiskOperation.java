package com.epam.gomel.tat.yandex.product.logic.disk;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.util.Wait;
import com.epam.gomel.tat.yandex.product.pages.disk.UserDiskPage;

import java.util.concurrent.TimeUnit;

public class DiskOperation extends BaseTest {

    public static void choiceNavigateItem(String item) {
        Log.info("Open" + item + "Navigate");
        UserDiskPage.navigate(item).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Wait.waitForJSLoads();
        Log.debug("Screen");
    }
}
