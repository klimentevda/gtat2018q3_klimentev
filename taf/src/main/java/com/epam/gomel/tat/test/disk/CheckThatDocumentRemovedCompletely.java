package com.epam.gomel.tat.test.disk;


import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.bo.AccountFectory;
import com.epam.gomel.tat.framework.util.WindowsWork;
import com.epam.gomel.tat.yandex.product.logic.disk.CreateNewFileOperation;
import com.epam.gomel.tat.yandex.product.logic.disk.DiskOperation;
import com.epam.gomel.tat.yandex.product.logic.disk.FileTrashOperation;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.TrashPage;
import com.epam.gomel.tat.yandex.product.servise.AccountServise;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckThatDocumentRemovedCompletely extends BaseTest {
    /**
     * Test you can empty trash. Check that document removed completely
     */
    @Test
    public static void moveToTrash() {
        new AccountServise().sinlIn(AccountFectory.defoultAccount());
        CreateNewFileOperation.openDisk();
        CreateNewFileOperation.newFolderFile();
        CreateNewFileOperation.openNewFolder();
        CreateNewFileOperation.createWordFile();
        CreateNewFileOperation.newInputWordinWordFile("Hello world!");
        WindowsWork.qthePage();
        FileTrashOperation.moveElementtoTrash();

        Assert.assertEquals(
                TrashPage.checkItemsFile().getText(),
                "",
                "New word file don`t be in trash");
        DiskOperation.choiceNavigateItem("trash");
        FileTrashOperation.cleanTrash();

        Assert.assertEquals(
                TrashPage.checkItemsFile().getText(),
                "",
                "New word file don`t be in trash");
    }
}
