package com.epam.gomel.tat.yandex.product.servise;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.yandex.product.pages.LoginPageYandex;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BaseTest {

    public static String idLevel = LoginPageYandex.idLevel().getAttribute("class");
    public static WebElement dynamicElement;

    public static void typeLogin(String login) {
        if (idLevel.equals("layout")) {
            LoginPageYandex.userName2().sendKeys(login);
            LoginPageYandex.userButtonIn2().click();
        } else {
            LoginPageYandex.userName().sendKeys(login);
        }
    }

    public static void typePassword(String password) {
        if (idLevel.equals("layout")) {
            dynamicElement = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name= 'passwd']")));
            LoginPageYandex.userPass().click();
            LoginPageYandex.userPass().sendKeys(password);
            LoginPageYandex.userButtonIn2().click();
        } else {
            LoginPageYandex.checkbox().click();
            LoginPageYandex.userPass().sendKeys(password);
            LoginPageYandex.userButtonIn().click();
        }
    }
}
