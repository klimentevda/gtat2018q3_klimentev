package com.epam.gomel.tat.yandex.product.logic.disk;

import com.epam.gomel.tat.core.BaseTest;
import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.util.RandomString;
import com.epam.gomel.tat.framework.util.Wait;
import com.epam.gomel.tat.framework.util.WindowsWork;
import com.epam.gomel.tat.yandex.product.pages.disk.WordPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.DiskPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create.CreateNewFolderPage;
import com.epam.gomel.tat.yandex.product.pages.disk.navigation.file.create.NewFolderPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.ThreadLocalRandom;


public class CreateNewFileOperation extends BaseTest {

    private static RandomString gen = new RandomString(5, ThreadLocalRandom.current());
    private static Actions builder = new Actions(driver);
    private static String nameGen = gen.nextString();
    private static WebDriverWait wait = new WebDriverWait(driver, 10);

    public static String getNameGen() {
        return nameGen;
    }

    public static void newFolderFile() {
        Log.info("Name folder = " + nameGen);
        Wait.waitEndClic(DiskPage.newButton());
        Wait.waitEndClic(DiskPage.newFolder());
        CreateNewFolderPage.newNameFolder().sendKeys(Keys.CONTROL + "a");
        CreateNewFolderPage.newNameFolder().sendKeys(Keys.DELETE);
        Wait.waitForJSLoads();
        Wait.waitEndClear(CreateNewFolderPage.newNameFolder());
        wait.until(ExpectedConditions.textToBePresentInElementValue(CreateNewFolderPage.newNameFolder(), ""));
        Wait.waiteString(CreateNewFolderPage.newNameFolder(), "");
        Log.info("Clear old name folder");
        Wait.waitEndSendKey(CreateNewFolderPage.newNameFolder(), nameGen);
        Wait.waitEndClic(CreateNewFolderPage.saveButtonnewFolder());
        Wait.waiteString(DiskPage.detectNewFolder(), "");
        Wait.waitForJSLoads();
    }

    public static void openNewFolder() {
        Wait.waitForJSLoads();
        driver.get("https://disk.yandex.ru/client/disk/" + nameGen);
        Log.info("Open folder - " + nameGen);
        wait.until(ExpectedConditions.elementToBeClickable(NewFolderPage.newNameFolder()));
        Wait.waitForJSLoads();
    }

    public static void openDisk() {
        Wait.waitForJSLoads();
        driver.get("https://disk.yandex.ru/client/disk/");
        Log.info("Open dick ");
        wait.until(ExpectedConditions.elementToBeClickable(NewFolderPage.newNameFolder()));
        Wait.waitForJSLoads();
    }

    public static void newInputWordinWordFile(String input) {
        Wait.waitForJSLoads();
        builder.sendKeys(WordPage.stringWord(), input).perform();
        Wait.waiteString(WordPage.status(), "Сохранено в Yandex");
    }

    public static void openLastWordFile() {
        Wait.waitForJSLoads();
        builder.doubleClick(NewFolderPage.lastWord()).perform();
        builder.click(WordPage.linkEdit()).perform();
        wait.until(ExpectedConditions.elementToBeClickable(WordPage.stringWord()));
        WindowsWork.choseThePage();
        wait.until(ExpectedConditions.titleContains("Документ"));
        WindowsWork.choseTheframe();
    }

    public static void createWordFile() {
        Wait.waitForJSLoads();
        Wait.waitEndClic(NewFolderPage.newButton());
        Wait.waitEndClic(NewFolderPage.newWordDocument());
        WindowsWork.choseThePage();
        wait.until(ExpectedConditions.titleContains("Документ"));
        WindowsWork.choseTheframe();
    }
}
